CREATE TABLE IF NOT EXISTS financial_operations(
  id bigserial not null primary key,
  user_id bigint,
  state text,
  schema bigint not null,
  version int not null,
  data json
);

CREATE INDEX IF NOT EXISTS financial_operations_user_idx ON financial_operations(user_id);
