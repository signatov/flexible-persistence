package com.example.demo.model;

import com.example.demo.model.view.DB;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "class")
public class OldFinancialOperation {

    public static final int SCHEMA = 1;

    private Long id;
    private Long userId;
    private String state;
    private Integer version;

    @JsonView(DB.class) private BigDecimal amount;
    @JsonView(DB.class) private BigDecimal reductionFactor;

    public OldFinancialOperation(long id, Long userId, String state, Integer version) {
        this.id = id;
        this.userId = userId;
        this.state = state;
        this.version = version;
    }

    public OldFinancialOperation(Long userId, String state) {
        this.userId = userId;
        this.state = state;
        this.version = 1;
    }
}
