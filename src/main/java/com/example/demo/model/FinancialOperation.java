package com.example.demo.model;

import com.example.demo.model.view.DB;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BtcFinancialOperation.class)
})
@NoArgsConstructor
public class FinancialOperation {

    public static final int SCHEMA = 2;

    private Long id;
    private Long userId;
    private String state;
    private Integer version;

    @JsonView(DB.class) private BigDecimal amount;
    @JsonView(DB.class) private List<String> params;

    public FinancialOperation(long id, Long userId, String state, Integer version) {
        this.id = id;
        this.userId = userId;
        this.state = state;
        this.version = version;
    }

    public FinancialOperation(Long userId, String state) {
        this.userId = userId;
        this.state = state;
        this.version = 1;
    }

    private FinancialOperation(final @NotNull OldFinancialOperation operation) {
        this.id = operation.getId();
        this.userId = operation.getUserId();
        this.state = operation.getState();
        this.version = operation.getVersion();
        this.amount = operation.getAmount();
    }

    public static FinancialOperation renew(final @NotNull OldFinancialOperation operation) {
        return new FinancialOperation(operation);
    }
}
