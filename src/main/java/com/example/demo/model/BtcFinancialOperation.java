package com.example.demo.model;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class BtcFinancialOperation extends FinancialOperation {
    public BtcFinancialOperation(Long userId, String state) {
        super(userId, state);
    }
}
