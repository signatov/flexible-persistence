package com.example.demo.repository;

import com.example.demo.model.FinancialOperation;
import com.example.demo.model.OldFinancialOperation;
import com.example.demo.model.view.DB;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Repository
@Slf4j
public class FinancialOperationRepository {
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Queries {
        static final String SELECT =
                "SELECT schema, id, user_id, state, version, data FROM financial_operations WHERE user_id = ?";

        static final String SELECT_ALL =
                "SELECT schema, id, user_id, state, version, data FROM financial_operations";

        static final String INSERT =
                "INSERT INTO financial_operations(user_id, state, schema, version, data) values (?, ?, ?, ?, cast(? as json))";

        static final String INSERT_ID =
                "INSERT INTO financial_operations(user_id, state, schema, version, data) values (?, ?, ?, ?, cast(? as json)) RETURNING id";

        static final String UPDATE =
                "UPDATE financial_operations SET schema = ?, state = ?, data = cast(? as json), version = version + 1 WHERE id = ? AND version = ?";

        static final String TOTAL_COUNT =
                "SELECT COUNT(id) FROM financial_operations";
    }

    private final JdbcTemplate jdbcTemplate;
    private final ObjectMapper objectMapper;

    private JdbcTemplate jdbc() { return jdbcTemplate; }

    public void add(final @NotNull FinancialOperation operation) {
        jdbc().update(Queries.INSERT, operation.getUserId(), operation.getState(),
                FinancialOperation.SCHEMA, operation.getVersion(), toJson(operation));
    }

    public FinancialOperation addWithId(final @NotNull FinancialOperation operation) {
        final KeyHolder key = new GeneratedKeyHolder();
        jdbc().update(conn -> {
            final PreparedStatement st = conn.prepareStatement(Queries.INSERT_ID, Statement.RETURN_GENERATED_KEYS);
            st.setLong(1, operation.getUserId());
            st.setString(2, operation.getState());
            st.setInt(3, FinancialOperation.SCHEMA);
            st.setInt(4, operation.getVersion());
            st.setString(5, toJson(operation));
            return st;
        }, key);
        operation.setId(Optional.ofNullable(key.getKey()).map(Number::longValue).orElse(0L));
        return operation;
    }

    public void add(final @NotNull OldFinancialOperation operation) {
        jdbc().update(Queries.INSERT, operation.getUserId(), operation.getState(),
                OldFinancialOperation.SCHEMA, operation.getVersion(), toJson(operation));
    }

    public void batchAdd(final @NotNull List<FinancialOperation> operations) {
        List<Object[]> objects = operations.stream().filter(Objects::nonNull).map(this::flat).collect(Collectors.toList());
        jdbc().batchUpdate(Queries.INSERT, objects);
    }

    public List<FinancialOperation> findAll() {
        return find(Queries.SELECT_ALL, null);
    }

    public List<FinancialOperation> findByUserId(final @NotNull Long userId) {
        return find(Queries.SELECT, new Object[]{ userId });
    }

    public FinancialOperation update(final @NotNull FinancialOperation operation) {
        int rowsAffected = jdbc().update(Queries.UPDATE, FinancialOperation.SCHEMA, operation.getState(),
                toJson(operation), operation.getId(), operation.getVersion());
        if (rowsAffected > 0) {
            operation.setVersion(operation.getVersion() + 1);
        }
        return operation;
    }

    public Long totalCount() {
        return jdbc().queryForObject(Queries.TOTAL_COUNT, Long.class);
    }

    public boolean emptySet() {
        return totalCount() == 0L;
    }

    private List<FinancialOperation> find(final @NotNull String query, Object[] args) {
        return jdbc().query(query, args, (rs, i) -> {
            int schema = rs.getInt(1);
            long id = rs.getLong(2);
            Long uid = rs.getLong(3);
            String state =  rs.getString(4);
            Integer version = rs.getInt(5);
            String data = rs.getString(6);
            if (schema == OldFinancialOperation.SCHEMA) {
                OldFinancialOperation operation = new OldFinancialOperation(id, uid, state, version);
                fromJson(operation, data);
                return FinancialOperation.renew(operation);
            }
            return read(id, uid, state, version, data);
        });
    }

    private FinancialOperation read(long id, Long uid, String state, Integer version, String data) {
        try {
            FinancialOperation operation = objectMapper.readerFor(FinancialOperation.class).readValue(data);
            operation.setId(id);
            operation.setUserId(uid);
            operation.setState(state);
            operation.setVersion(version);
            return operation;
        } catch (IOException ignore) {
            return new FinancialOperation(id, uid, state, version);
        }
    }

    private Object[] flat(final @NotNull FinancialOperation operation) {
        return new Object[] {
                operation.getUserId(), operation.getState(),
                FinancialOperation.SCHEMA, operation.getVersion(),
                toJson(operation)
        };
    }

    private String toJson(final @NotNull Object operation) {
        try {
            return objectMapper.writerWithView(DB.class).writeValueAsString(operation);
        } catch (JsonProcessingException e) {
            log.error("Serialize or I/O error", e);
        }
        return null;
    }

    private void fromJson(final @NotNull Object operation, final @NotNull String json) {
        try {
            objectMapper.readerForUpdating(operation).readValue(json);
        } catch (IOException e) {
            log.error("Deserialize or I/O error", e);
        }
    }
}
