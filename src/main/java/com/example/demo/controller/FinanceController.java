package com.example.demo.controller;

import com.example.demo.model.FinancialOperation;
import com.example.demo.repository.FinancialOperationRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/finance")
@RequiredArgsConstructor
@Slf4j
public class FinanceController {
    private final FinancialOperationRepository repository;

    @GetMapping("/all")
    public ResponseEntity<List<FinancialOperation>> listAll() {
        return ResponseEntity.of(Optional.of(repository.findAll()));
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<List<FinancialOperation>> list(@PathVariable long userId) {
        return ResponseEntity.of(Optional.of(repository.findByUserId(userId)));
    }
}
