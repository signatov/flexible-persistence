package com.example.demo;

import com.example.demo.model.BtcFinancialOperation;
import com.example.demo.model.FinancialOperation;
import com.example.demo.model.OldFinancialOperation;
import com.example.demo.repository.FinancialOperationRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class DataFiller implements CommandLineRunner {
    private static final String INSERT_QUERY =
            "INSERT INTO financial_operations(user_id, state, schema, version, data) values (?, ?, ?, ?, cast(? as json))";

    private final JdbcTemplate jdbcTemplate;
    private final FinancialOperationRepository operationRepository;

    private JdbcTemplate jdbc() { return jdbcTemplate; }

    @Override
    public void run(String... args) {
        if (!operationRepository.emptySet()) {
            return;
        }

        insertRawData();

        addOldVersionOperation(2L);

        addBtcOperation(2L);

        decreaseAllAmounts(2L);

        addAndUpdateOperation(2L);
    }

    private void decreaseAllAmounts(long userId) {
        List<FinancialOperation> ops = operationRepository.findByUserId(userId);
        for (final FinancialOperation op : ops) {
            log.info(op.toString());

            op.setAmount(op.getAmount().subtract(BigDecimal.ONE));
            operationRepository.update(op);
        }
    }

    private void addAndUpdateOperation(long userId) {
        FinancialOperation op = new FinancialOperation(userId, "not_init");
        op.setAmount(BigDecimal.ZERO);
        op.setParams(Arrays.asList("btc", "eth", "xrp", "nem", "tron"));
        operationRepository.add(op);

        op.setAmount(BigDecimal.TEN);
        op.setState("init");
        op = operationRepository.update(op);

        log.info(op.toString());
    }

    private void addOldVersionOperation(long userId) {
        final OldFinancialOperation op = new OldFinancialOperation(userId, "processed");
        op.setAmount(BigDecimal.TEN);
        op.setReductionFactor(BigDecimal.ONE);
        operationRepository.add(op);
    }

    private void addBtcOperation(long userId) {
        final BtcFinancialOperation btcOp = new BtcFinancialOperation(userId, "failed");
        btcOp.setAmount(BigDecimal.valueOf(666));
        final FinancialOperation op = operationRepository.addWithId(btcOp);
        log.info("inserted {}", op);
    }

    private void insertRawData() {
        List<Object[]> objs = Arrays.asList(
                newOp(1, 100), newOp(2, 200), newOp(3, 300)
        );
        jdbc().batchUpdate(INSERT_QUERY, objs);
    }

    private Object[] newOp(final long userId, final long sum) {
        final String klass = userId % 2 != 0 ? "com.example.demo.model.FinancialOperation" : "com.example.demo.model.BtcFinancialOperation";
        final String data = "{\"class\":\"%s\", \"amount\": %d, \"params\": [\"btc\", \"eth\"]}";
        return new Object[] {userId, "init", FinancialOperation.SCHEMA, 1, String.format(data, klass, sum)};
    }
}
